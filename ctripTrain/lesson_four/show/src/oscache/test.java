package oscache;

public class test {

	public test() {
		//memoryCache();
		diskCache();
		//hashDiskCache();
	}
	
	public void hashDiskCache(){
		OSHashDiskCache dcache=new OSHashDiskCache("d:\\bbb");
        dcache.put("jeffery", 40);
		System.out.println(dcache.getFilePath());
		System.out.println(dcache.get("jeffery"));	
	}
	
	public void diskCache(){
		OSDiskCache dcache=new OSDiskCache("d:\\999");
        dcache.put("jeffery", 40);
		System.out.println(dcache.getFilePath());
		System.out.println(dcache.get("jeffery"));
	}
	
	public void memoryCache(){
		OSMemoryCache bcache=new OSMemoryCache(-1);
		bcache.put("jeffery", 30);
		OSMemoryCache ccache=new OSMemoryCache(10);
		ccache.put("tina", 2000000);
		for(int i=0;i<20;i++){
			System.out.println("jeffery=" + bcache.get("jeffery"));
			System.out.println("tina=" + ccache.get("tina"));
			try{Thread.sleep(1000);
			}catch(InterruptedException e){e.printStackTrace();}
		}
	}

	public static void main(String[] args) {new test();}
}
package oscache;

import java.util.Properties;

import com.opensymphony.oscache.base.Config;
import com.opensymphony.oscache.base.persistence.CachePersistenceException;
import com.opensymphony.oscache.plugins.diskpersistence.DiskPersistenceListener;

public class OSDiskCache extends DiskPersistenceListener{

	private static final long serialVersionUID = 19761210;
	
	private String filePath=null;
	
	/**
	 * @param filePath ���̴洢·��
	 */
	public OSDiskCache(String filePath){
		this.filePath=filePath;
        Properties p = new Properties();
        p.setProperty("cache.path", this.filePath);
        p.setProperty("cache.memory", "false");
        p.setProperty("cache.persistence.class", "com.opensymphony.oscache.plugins.diskpersistence.DiskPersistenceListener");
        this.configure(new Config(p));
	}
	
	/**
	 * �õ��洢·��
	 * @return
	 */
	public String getFilePath(){
		return this.getCachePath().toString();
	}
	
	public void put(String key,Object value){
		try{this.store(key, value);
		}catch(CachePersistenceException e){e.printStackTrace();}
	}
	
	public Object get(String key){
		Object obj=null;
		try{obj=this.retrieve(key);
		}catch(CachePersistenceException e){e.printStackTrace();}
		return obj;
	}
	
	public void remove(String key){
		try{super.remove(key);
		}catch(CachePersistenceException e){e.printStackTrace();}
	}
}
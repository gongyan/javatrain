package oscache;

import java.util.Date;
import com.opensymphony.oscache.base.NeedsRefreshException;
import com.opensymphony.oscache.general.GeneralCacheAdministrator;

public class OSMemoryCache extends GeneralCacheAdministrator{
	
	private static final long serialVersionUID = 19761210;
	
	private int expiredSecond; //过期时间(单位为秒);
	
	/**
	 * 过期时间(单位为秒);负数不过期
	 * @param expiredSecond
	 */
	public OSMemoryCache(int expiredSecond){
		super();
		this.expiredSecond = expiredSecond;
	}
	
	//添加被缓存的对象;
	public void put(String key,Object value){
		this.putInCache(key,value);
	}

	//删除被缓存的对象;
	public void remove(String key){
		this.flushEntry(key);
	}

	//删除所有被缓存的对象;
	public void removeAll(Date date){
		this.flushAll(date);
	}

	public void removeAll(){
		this.flushAll();
	}

	//获取被缓存的对象;
	public Object get(String key){
		Object obj=null;
		try{obj=this.getFromCache(key,this.expiredSecond);
		}catch(NeedsRefreshException e){this.cancelUpdate(key);}
		return obj;
	}
}
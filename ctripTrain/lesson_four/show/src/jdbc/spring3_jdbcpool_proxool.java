package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.logicalcobwebs.proxool.ProxoolDataSource;
import org.logicalcobwebs.proxool.ProxoolException;
import org.logicalcobwebs.proxool.ProxoolFacade;
import org.logicalcobwebs.proxool.admin.SnapshotIF;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class spring3_jdbcpool_proxool {

	private static int activeCount = 0;
	
	public spring3_jdbcpool_proxool() {
		Connection conn=null;
        ApplicationContext appCt = new ClassPathXmlApplicationContext("database.xml");
        ProxoolDataSource dataSource = (ProxoolDataSource) appCt.getBean("proxoolDataSource");
		try{
			conn = dataSource.getConnection();
			try{
	            SnapshotIF snapshot = ProxoolFacade.getSnapshot("test", true);
	            int curActiveCount=snapshot.getActiveConnectionCount();//获得活动连接数
	            if(curActiveCount!=activeCount) activeCount=curActiveCount;
	        }catch(ProxoolException e){e.printStackTrace();}
			conn.setAutoCommit(false);
		}catch(SQLException e){e.printStackTrace();}
		ResultSet rs=null;
		PreparedStatement stmt=null;
		try{
			String sql="select * from mytable";
			stmt=conn.prepareStatement(sql);
			rs=stmt.executeQuery();
			while(rs.next()){ 
				if(rs.getString(1)!=null)
			      System.out.println("username=" + rs.getString("username") + " age=" + rs.getInt("age"));
			}
			conn.commit();
    		conn.setAutoCommit(true); 
		}catch(SQLException e){e.getMessage();
		}finally{try {rs.close();stmt.close();}catch(SQLException e){e.getMessage();}}
	}

	public static void main(String[] args) {
		new spring3_jdbcpool_proxool();
	}
}
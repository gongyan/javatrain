package jdbc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class blobsql {

	private String url="jdbc:mysql://localhost:3306/test?zeroDateTimeBehavior=convertToNull&amp;characterEncoding=utf8"; 
	private String user="root"; 
	private String password="root"; 
	
	public blobsql() {
		savefile();
		readfile();
	}
	
	//保存大对象到数据库
	public void savefile(){
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance(); 
			Connection conn= DriverManager.getConnection(url,user,password);
			PreparedStatement ps = conn.prepareStatement("insert into blobtable values (?,?)");
			ps.setString(1, "aaa.txt");
			InputStream in=null;
			try {
				in = new FileInputStream("d:/abc.txt");
				ps.setBinaryStream(2,in,in.available());
			}catch (FileNotFoundException e) {
				e.printStackTrace();
			}catch (IOException e) {
				e.printStackTrace();
			}
		    ps.executeUpdate();
		    ps.close();
		    conn.close();
		}catch(ClassNotFoundException e){
		  System.out.println("Sorry!Class not found!");
		}catch(InstantiationException e){
		  System.out.println("Sorry!Oracle engine not found!");
		}catch(IllegalAccessException e){
		  System.out.println("Class can not be initialized!");
		}catch(SQLException e){
		  System.out.println("SQLException is found!");
		}
	}
	
	//读取文件
	public void readfile(){
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance(); 
			Connection conn= DriverManager.getConnection(url,user,password);
			PreparedStatement ps = conn.prepareStatement("select * from blobtable where filename = ?");
	        ps.setString(1, "aaa.txt");
	        ResultSet rs = ps.executeQuery();
	        rs.next();
	        InputStream in = rs.getBinaryStream("file");
	        FileOutputStream out = new FileOutputStream("d:/abc1.txt");
	        byte[] b = new byte[1024];
	        int len = 0;
	        while ( (len = in.read(b)) != -1) {
	          out.write(b, 0, len);
	          out.flush();
	        }
	        out.close();
	        in.close();
	        rs.close();
	        ps.close();
		}catch(ClassNotFoundException e){
		  System.out.println("Sorry!Class not found!");
		}catch(InstantiationException e){
		  System.out.println("Sorry!Oracle engine not found!");
		}catch(IllegalAccessException e){
		  System.out.println("Class can not be initialized!");
		}catch(SQLException e){
		  System.out.println(e.getMessage());
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new blobsql();
	}
}
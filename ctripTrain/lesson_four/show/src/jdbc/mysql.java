package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.CallableStatement;

public class mysql {

	public mysql() {
		try{
			  //得到conn对象
			  Class.forName("com.mysql.jdbc.Driver").newInstance(); 
			  String url="jdbc:mysql://localhost:3306/test?zeroDateTimeBehavior=convertToNull&amp;characterEncoding=utf8"; 
			  String user="root"; 
			  String password="root"; 
			  Connection conn= DriverManager.getConnection(url,user,password);
			  
			  //查询记录
			  String sql="select * from mytable where username=?";
			  PreparedStatement stmt=conn.prepareStatement(sql);
			  stmt.setString(1,"tina");
			  ResultSet rs=stmt.executeQuery();
			  while(rs.next()){ 
			    if(rs.getString(1)!=null)
			      System.out.println("username=" + rs.getString("username") + " age=" + rs.getInt("age"));
			  }
			  
			  //更新记录
			  sql="update mytable set age=100 where username=?";
			  stmt=conn.prepareStatement(sql);
			  stmt.setString(1,"tina");
			  stmt.executeUpdate();
			  
			  //调用存储过程
			  CallableStatement cStmt= (CallableStatement)conn.prepareCall("call inserter('aaaa',1111)");
			  cStmt.executeUpdate();
			  
			  cStmt.close();
			  rs.close(); 
			  stmt.close(); 
			  conn.close();
			}catch(ClassNotFoundException e){
			  System.out.println("Sorry!Class not found!");
			}catch(InstantiationException e){
			  System.out.println("Sorry!Oracle engine not found!");
			}catch(IllegalAccessException e){
			  System.out.println("Class can not be initialized!");
			}catch(SQLException e){
			  System.out.println("SQLException is found!");
			}
	}

	public static void main(String[] args) {
		new mysql();
	}
}

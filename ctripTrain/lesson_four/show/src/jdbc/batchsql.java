package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class batchsql {

	//批处理sql
	public batchsql() {
		try{
			  //得到conn对象
			  Class.forName("com.mysql.jdbc.Driver").newInstance(); 
			  String url="jdbc:mysql://localhost:3306/test?zeroDateTimeBehavior=convertToNull&amp;characterEncoding=utf8"; 
			  String user="root"; 
			  String password="root"; 
			  Connection conn= DriverManager.getConnection(url,user,password);
			  
			  //StatementBatch演示,sql语句可以不同
			  String sql1="insert into mytable(username,age) values('statementbatch1',123)";
			  String sql2="update mytable set age=999 where username='statementbatch1'";
			  Statement st=conn.createStatement();
			  st.addBatch(sql1);//插入第一条sql执行语句
			  st.addBatch(sql2);//插入第二条sql执行语句
			  st.executeBatch();
			  st.close();
			  
			  //PreparestatementBatch演示,sql语句除了参数不同，语句体必须相同
			  String sql3="insert into mytable(username,age) values(?,?)";
			  PreparedStatement psmt=conn.prepareStatement(sql3);
			  psmt.setString(1, "preparestatementbatch2");
			  psmt.setInt(2, 1000);
			  psmt.addBatch();
			  psmt.setString(1, "preparestatementbatch3");
			  psmt.setInt(2, 1001);
			  psmt.addBatch();
			  psmt.executeBatch();
			  psmt.clearBatch();
			  psmt.close();
			  conn.close();
			}catch(ClassNotFoundException e){
			  System.out.println("Sorry!Class not found!");
			}catch(InstantiationException e){
			  System.out.println("Sorry!Oracle engine not found!");
			}catch(IllegalAccessException e){
			  System.out.println("Class can not be initialized!");
			}catch(SQLException e){
			  System.out.println("SQLException is found!");
			}
	}

	public static void main(String[] args) {
		new batchsql();
	}
}
package encache;

import net.sf.ehcache.Cache;  
import net.sf.ehcache.CacheManager;  
import net.sf.ehcache.Element;  

public class ehcacheTest {  

	public static void main(String[] args) throws InterruptedException {
		//创建缓存管理器    
		CacheManager manager = CacheManager.create("src/ehcache.xml");    
		//取出所有的cacheName  
		String names[] = manager.getCacheNames();  
		for(int i=0;i<names.length;i++){  
			System.out.println(names[i]);  
		}  
		//根据cacheName生成一个Cache对象 
		Cache cache=manager.getCache(names[0]);     
		//向Cache对象里添加Element元素，Element元素有key，value键值对组成  
		cache.put(new Element("key1","values1"));  
		Element element = cache.get("key1");  
		System.out.println(element.getValue());  
		Object obj = element.getObjectValue();  
		System.out.println((String)obj);  
		manager.shutdown();
	}  
}
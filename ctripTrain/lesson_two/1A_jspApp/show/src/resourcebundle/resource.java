package resourcebundle;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class resource {

	public resource() {
		//指定语言和国家
		Locale currentLocale = new Locale("zh", "CH");
		//指定properties文件的前缀
		ResourceBundle myResources = ResourceBundle.getBundle("labels", currentLocale);
		//获取key值
		String value = myResources.getString("submit");
		System.out.println(value);
		
		//创建MessageFormat对象，并设置其locale属性
		MessageFormat formatter = new MessageFormat("");
		formatter.setLocale(currentLocale);
		ResourceBundle messages=ResourceBundle.getBundle("labels",currentLocale);
		//设置参数
		Object[] testArgs = {"毛泽东"};
		System.out.println(formatter.format(messages.getString("welcome"),testArgs));
	}
	
	public static void main(String[] args){
		new resource();
	}

}

package servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * 初始化环境变量加载
 */
public class InitOtherConfigServlet extends HttpServlet {
	private static final long serialVersionUID = 19761210;
	

	public void init(ServletConfig arg0) throws ServletException {
		String parm1=arg0.getInitParameter("SPLIT_COLUMN_LENGTH");
		String parm2=arg0.getInitParameter("PHYSICAL_PATH");
		System.out.println("SPLIT_COLUMN_LENGTH="+parm1);
		System.out.println("PHYSICAL_PATH="+parm2);
	}

	public void destroy(){super.destroy();}
}
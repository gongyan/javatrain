<%@page language="java" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <title>demo Parameter Encoding</title>
  </head>  
  <body>
 用户名：<c:out value="${param.username}" default="none"/><br>
 密码：<c:out value="${param.password}" default="none"/><br>
    <form action="CharacterEnccodingFilter.jsp" method="post">
      <table border="0">
        <tr>
          <td>用户名:</td>
          <td><input type="text" name="username"></td>
        </tr>
        <tr>
          <td>密码:</td>
          <td><input type="password" name="password"></td>
        </tr>
        <tr>
          <td colspan="2" align="center"><input type="submit"></td>
        </tr>
      </table>
    </form>
    <br>http://localhost:8080/show/CharacterEnccodingFilter.jsp
  </body>
</html>
<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8"%>
<html>  
  <body><br>
<%
//使用pageContext设置属性，该属性默认在page范围内
pageContext.setAttribute("name","page_test");
request.setAttribute("name","request_test");
session.setAttribute("name","session_test");
application.setAttribute("name","application_test");
%>
page设定的值:<%=pageContext.getAttribute("name")%><br>
request设定的值：<%=pageContext.getRequest().getAttribute("name")%><br>
session设定的值：<%=pageContext.getSession().getAttribute("name")%><br>
application设定的值：<%=pageContext.getServletContext().getAttribute("name")%><br>
范围1内的值：<%=pageContext.getAttribute("name",1)%><br>
范围2内的值：<%=pageContext.getAttribute("name",2)%><br>
范围3内的值：<%=pageContext.getAttribute("name",3)%><br>
范围4内的值：<%=pageContext.getAttribute("name",4)%><br>
<!--从最小的范围page开始，然后是reques、session以及application-->
<%pageContext.removeAttribute("name",1);%>
pageContext修改后的session设定的值：<%=session.getValue("name")%><br>
<%pageContext.setAttribute("name","test",4);%>
pageContext修改后的application设定的值：<%=pageContext.getServletContext().getAttribute("name")%><br>
pageContext.findAttribute工作原理：先在page scope里找，如果找不到就到request scope里，
再找不到就到session scope(if valid)里找，再找不到就到application scope(s)里找，再找不到就返回null。<br>
值的查找：<%=pageContext.findAttribute("name")%><br>
当前查找的范围
属性name的范围：<%=pageContext.getAttributesScope("name")%><br>
<br>http://localhost:8080/show/pageContext.jsp
</body>
</html>

<%@ page contentType="text/html; charset=GBK" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${param.language == 'zh' || param.language == null }">
  <fmt:setLocale value="zh" scope="page"/>
</c:if>
<c:if test="${param.language == 'en' }">
  <fmt:setLocale value="en" scope="page"/>
</c:if>
<c:if test="${param.language == 'sv'}">
  <fmt:setLocale value="sv" scope="page"/>
</c:if>
<c:if test="${param.language == 'de'}">
  <fmt:setLocale value="de" scope="page"/>
</c:if>
<fmt:setBundle basename="labels" scope="page"/>
<html>
<head>
<title>
<fmt:message key="title"/>
</title>
</head>
<body bgcolor="white">
<h1>
  <fmt:message key="title"/>
</h1>
<fmt:message key="select_language"/>:
<form action="I18N.jsp" method="POST">
<p>
  <c:choose>
    <c:when test="${param.language == 'zh'}">
      <input type="radio" name="language" value="zh" checked="checked">
    </c:when>
    <c:otherwise>
      <input type="radio" name="language" value="zh" >
    </c:otherwise>
  </c:choose>
 <fmt:message key="chinese"/>
  <br>
  <c:choose>
    <c:when test="${param.language == 'en'}">
      <input type="radio" name="language" value="en" checked="checked">
    </c:when>
    <c:otherwise>
      <input type="radio" name="language" value="en" >
    </c:otherwise>
  </c:choose>
  <fmt:message key="english"/>
  <br>
  <c:choose>
    <c:when test="${param.language == 'sv'}">
      <input type="radio" name="language" value="sv" checked="checked">
    </c:when>
    <c:otherwise>
      <input type="radio" name="language" value="sv" >
    </c:otherwise>
  </c:choose>
  <fmt:message key="swedish"/>
  <br>
  <c:choose>
    <c:when test="${param.language == 'de'}">
      <input type="radio" name="language" value="de" checked="checked">
    </c:when>
    <c:otherwise>
      <input type="radio" name="language" value="de" >
    </c:otherwise>
  </c:choose>
  <fmt:message key="german"/>
  <br>
<p>
  <input type="submit" value="<fmt:message key="new_language"/>" >
</form>
</body>
</html>
<br>http://localhost:8080/show/I18N.jsp

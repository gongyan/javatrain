package thread;

//public class MyThread extends Thread{
public class MyThread implements Runnable{

	public MyThread() {
		// TODO Auto-generated constructor stub
	}

	public void run(){
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doSth1();
		doSth2();
		doSth3();
		synchronized(this){
			doSth4();
			doSth5();
		}
	}
	
	public void doSth1(){
		System.out.println(Thread.currentThread().getName() + "call doSth1");
	}
	
	private synchronized void doSth2(){
		System.out.println(Thread.currentThread().getName() + "call doSth2");
	}
	
	private void doSth3(){
		System.out.println(Thread.currentThread().getName() + "call doSth3");
	}
	
	private void doSth4(){
		System.out.println(Thread.currentThread().getName() + "call doSth4");
	}
	
	private void doSth5(){
		System.out.println(Thread.currentThread().getName() + "call doSth5");
	}
}
